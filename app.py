#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#azure ,# aws-- elasticbeanstalk -application ---,# heruko


# In[ ]:


from flask import Flask ,request,render_template 
import pickle  
import numpy as np
app = Flask(__name__)
model = pickle.load(open('regression_model.pkl','rb'))
@app.route('/')
def home():
    return render_template('index.html')
@app.route('/predict',methods= ['POST'])
def predict():
    float_features = [float(x) for x in request.form.values()]  #list comphress  #[12,3,4,5]
    final_features =[np.array(float_features)]
    prediction = model.predict(final_features)
    print(prediction)
    
    output = round(prediction[0],2)
    
    return render_template('index.html',prediction_text="Predicted cost is $ {}:".format(output))
                           
if __name__ == '__main__':
    app.run()

